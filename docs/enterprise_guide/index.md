---
redirect_to: '../fleet_scaling/index.md'
remove_date: '2022-02-05'
---

This document was moved to [another location](../fleet_scaling/index.md).

<!-- This redirect file can be deleted after 2022-02-05. -->
<!-- Before deletion, see: https://docs.gitlab.com/ee/development/documentation/#move-or-rename-a-page -->
